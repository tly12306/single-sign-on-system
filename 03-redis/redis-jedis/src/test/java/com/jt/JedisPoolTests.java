package com.jt;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolTests {
    @Test
    public void testJedisPool01(){
       //1.创建jedis连接池(JedisPool)
        //构建连接池配置对象(可选)
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(1000);//设置最大连接数
        jedisPoolConfig.setMaxIdle(60);//设置最大空闲连接数
        JedisPool jedisPool=
                //new JedisPool("192.168.192.128", 6379);
                new JedisPool(jedisPoolConfig,"192.168.192.128", 6379);
       //2.从池中获取一个连接对象(Jedis)
        Jedis resource = jedisPool.getResource();
      //3.基于Jedis读写redis数据
        resource.set("poolName", "JedisPool");
        String poolName = resource.get("poolName");
        System.out.println(poolName);
       //4.释放资源
        resource.close();
        jedisPool.close();
    }
    @Test
    public void testJedisPool02(){
        //1.从池中获取一个连接对象(Jedis)
        Jedis resource = JedisDataSource.getConnection();
        //2.基于Jedis读写redis数据
        resource.set("poolName", "JedisPool");
        String poolName = resource.get("poolName");
        System.out.println(poolName);
        //3.释放资源
        resource.close();
    }
}
