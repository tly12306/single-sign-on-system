package com.jt;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 基于某个活动设计一个简易的投票系统
 * 1)定义活动(活动id)
 * 2)基于活动进行投票(票数要存储到redis)
 * 3)同一个人不能对这个活动进行重复投票
 * 4)可以获取投票总数以及哪些人参与了这个活动的投票
 */
public class VoteDemo01 {
    /**
     * 执行投票逻辑
     * @param activityId
     * @param userId
     * @return true表示投票成功
     */
    static boolean doVote(String activityId,String userId){
        //1.检查是已参与过投票
        Jedis jedis = JedisDataSource.getConnection();
        Boolean flag=jedis.sismember(activityId, userId);
        //2.基于检查结果，执行投票或取消投票
        if(flag){
            jedis.srem(activityId, userId);
            jedis.close();
            return false;
        }
        jedis.sadd(activityId, userId);
        jedis.close();
        return true;
    }
    /**
     * 获取并输出投票总数
     * @param activityId 活动id
     * @return 总票数
     */
    static Long doCount(String activityId){
        Jedis jedis = JedisDataSource.getConnection();
        Long scard = jedis.scard(activityId);
        jedis.close();
        return scard;
    }

    /**
     * 获取参与过投票的用户
     * @param activityId
     * @return
     */
    static Set<String> doGetUsers(String activityId){
        Jedis jedis = JedisDataSource.getConnection();
        Set<String> members = jedis.smembers(activityId);
        jedis.close();
        return members;
    }

    public static void main(String[] args) {
        //1.定义活动，用户id(将来是登录用户)
        String activityId="10001";
        String userId1="201";
        String userId2="202";
        //2.执行投票,key为活动id，值为用户id，将具体数据存储到redis的set集合。
        boolean flag=doVote(activityId,userId2);
        System.out.println("flag="+flag);
        //3.获取并输出投票总数
        Long count=doCount(activityId);
        System.out.println("count="+count);
        //4.获取并输出哪些人参与了投票
        Set<String> users=doGetUsers(activityId);
        System.out.println("users="+users);
    }
}
