package com.jt;

import com.jt.redis.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class StringRedisTemplateTests {
    /**
     * StringRedisTemplate是一个特殊的RedisTemplate对象，
     * 只是默认序列化方式为string方式的序列化
     */
    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 将blog对象以json串的方式写到redis数据库，
     * 并将其读出了进行输出
     */
    @Test
    void testBlogJson(){//课堂练习
        //1.构建Blog对象
        Blog blog=new Blog();
        blog.setId(100);
        blog.setTitle("hello redis");
        //2.将Blog对象以json方式写入到redis
        redisTemplate.setValueSerializer(RedisSerializer.json());
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("blog", blog);
        //3.读取数据并进行输出
        blog=(Blog)valueOperations.get("blog");
        System.out.println(blog);
    }

    @Test
    void testHashOper(){
        HashOperations<String, Object, Object> vo =
                redisTemplate.opsForHash();
        vo.put("point", "x", "100");
        vo.put("point", "y", "200");
        vo.put("point", "y", "300");

        Boolean aBoolean = vo.hasKey("point", "z");
        System.out.println(aBoolean);
        Map<Object, Object> point = vo.entries("point");
        System.out.println(point);
        Set<Object> keys = vo.keys("point");
        List<Object> values = vo.values("point");
        System.out.println(keys);
        System.out.println(values);
    }
    @Test
    void testStringOper(){
        ValueOperations<String, String> valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("id", "100");
        valueOperations.set("title", "hello redis");
        Long id = valueOperations.increment("id");
        String title = valueOperations.get("title");
        System.out.println(id);
        System.out.println(title);
    }
}
