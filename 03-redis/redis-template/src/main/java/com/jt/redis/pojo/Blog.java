package com.jt.redis.pojo;

import java.io.Serializable;

public  class Blog implements Serializable {
    private static final long serialVersionUID = 5761563757464717011L;
    private Integer id;
    private String title;
    //...

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
